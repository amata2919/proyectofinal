using System;

namespace proyectofinal_master
{
     class proyect
    {
        private string[] nombreempleado;
        private int[,] sueldoempleado;
        private int[] Total;

        public void informacion() 
        {
           nombreempleado=new String[4];

           sueldoempleado=new int[4,3];
           
            for(int a = 0; a < nombreempleado.Length; a++)
            {
                Console.Write("Nombre del empleado: ");
           
                nombreempleado[a]=Console.ReadLine();
           
                for(int b = 0; b < sueldoempleado.GetLength(1); b++) 
                {
                    Console.Write("Sueldo: ");
           
                    string linea;
           
                    linea = Console.ReadLine();
           
                   sueldoempleado[a,b]=int.Parse(linea);
                }
            }
        }

        public void sueldo()
        {
            Total = new int[4];
            for (int a = 0; a < sueldoempleado.GetLength(0); a++)
            {
                int suma = 0;
                for (int b = 0; b < sueldoempleado.GetLength(1); b++)
                {
                    suma = suma + sueldoempleado[a,b];
                }
                Total[a] = suma;
            }
        }

        public void pagotot() 
        {
            Console.WriteLine("Sueldos pagados por empleado: ");
            for(int f = 0; f < Total.Length; f++) 
            {
                Console.WriteLine(nombreempleado[f]+" tiene un total de: "+Total[f]);
            }
        }

        public void sueldomayor() 
        {
            int mm=Total[0];
            string nom=nombreempleado[0];
            for(int f = 0; f < Total.Length; f++) 
            {
                if (Total[f] > mm) 
                {
                   mm=Total[f];
                    nom=nombreempleado[f];
                }
            }
            Console.WriteLine("El empleado con mayor sueldo es {0}, con un sueldo de: {1} ", nom, mm);
        }

        static void Main(string[] args)
        {
            proyect p = new proyect();
            p.informacion();
            p.sueldo();
            p.pagotot();
            p.sueldomayor();
            Console.ReadKey();
        }
    }
}
